const path = require('path');
const CleanWebPackPlugin = require('clean-webpack-plugin');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = (env) => {

    return {
        entry: './Client/index.js',
        output: {
            filename: 'bundle.js',
            path: path.resolve(__dirname, 'public')
        },
        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.js$/,
                    loader: 'eslint-loader',
                    options: {
                        failOnWarning: true,
                        failOnerror: true
                    },
                    exclude: /node_modules/
                },
                {
                    test: /\.js$/,
                    loader: 'babel-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.s?css$/,
                    use: ['style-loader', 'css-loader', 'sass-loader'],
                    exclude: /node_modules/
                },
                {
                    test: /\.svg$/,
                    loader: 'url-loader',
                    exclude: /node_modules/
                }
            ]
        },
        plugins: [
            new CleanWebPackPlugin(['public'], { root: path.resolve(__dirname) }),
            new HtmlWebPackPlugin({
                template: './Client/index.html',
                favicon: './Client/favicon.ico',
                inject: false
            }),
            new webpack.NormalModuleReplacementPlugin(/config.json/, ((resource) => {
                console.log(env);
                if (env && env.NODE_ENV) {
                    // eslint-disable-next-line
                    resource.request = resource.request.replace('.json', `.${env.NODE_ENV}.json`);
                }
            })),
        ],
        devtool: 'cheap-module-eval-source-map',
        devServer: {
            contentBase: path.resolve(__dirname, 'public'),
            compress: true,
            host: '0.0.0.0',
            port: 8080,
            disableHostCheck: true,
        }
    };
};
