var axios = require('axios');
var MockAdapter = require('axios-mock-adapter');
import { findNote } from './note-service';
import config from '../../config.json';

describe('Note api service', () => {
    let mock;
    beforeEach(() => {
        mock = new MockAdapter(axios);
    });
    it('should return note from api when findNote is called with an id', async () => {
        const noteExpected = {
            id: 10,
            title: 'title note',
            content: 'content note'
        };
        mock.onGet(`${config.baseApiUrl}/notes/${noteExpected.id}`).reply(200, noteExpected);
        const note = await findNote(noteExpected.id);
        expect(note).toEqual(noteExpected);
    });
});
